import { server } from './serverUrl';
import axios from 'axios';


export const auth = async (payload) => {
  try {
    const auth = await axios.post(`${server}/login`, payload);
    return auth;
  }
  catch (err) {
    return err;
  }
}