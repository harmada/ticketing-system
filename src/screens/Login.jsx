import React, { useState } from 'react';
import { Form, Button, Container, Row } from 'react-bootstrap'
import "../styles/Login.css"

import { auth } from '../api/auth';



const Login = () => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const login = async () => {
    const payload = {
      email,
      password,
    }

    const result = await auth(payload);
    console.log('result', result);
  }

  return (
    <div className="login">
      <div className="login-container">
        <h1 className="login-header">Ticketing System</h1>
        <Container>
          <Row>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control 
                type="email" 
                placeholder="Enter email" 
                onChange={ (e) => setEmail(e.target.value) }
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control 
                type="password" 
                placeholder="Password"
                onChange={ (e) => setPassword(e.target.value) }
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group>
            <Button 
              variant="primary" 
              onClick={login}
            >
              Submit
            </Button>
          </Row>
        </Container>
      </div>
    </div>

  )
}

export default Login
